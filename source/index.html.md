---
title: API Reference

language_tabs: # must be one of https://git.io/vQNgJ
  - Code

toc_footers:
  - <a href='#'>Sign Up for a Developer Key</a>
  - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - errors

search: true
---

# Introduction

These are the Dev Docs for maintaining the AffiniPay marketing websites (LawPay, CPACharge, AffiniPay and AffiniPay for Associations.

# Jekyll

> To install cd to the directory of your project and run:

```Code
$ npm install
$ nvm use
$ rbenv local
$ bundle install

```
```Code
To start up localhost run:

$ npm run watch

```



To get started check out the QA branch from the repo you want to work on locally from BitBucket.
To get started refer to the README.md for each site.
<a href="https://bitbucket.org/dashboard/overview">AffiniPay Websites</a>  
**LawPay** - <a href="http://localhost:8080">http://localhost:8080</a>  
**CPACharge** - <a href="http://localhost:4001">http://localhost:4001</a>   
**AP for Associations** -  <a href="http://localhost:8888">http://localhost:8888</a>   
**AffiniPay** - TBD  

<aside class="notice">
This project requires Node 6.x or greater.
</aside>

## config.yml
```Code
EXAMPLE CONFIG

# Site settings
title: CPACharge
email: re@name.me
description: > # this means to ignore newlines until "baseurl:"
  Add a description
baseurl: "" # the subpath of your site, e.g. /blog
url: "https://www.cpacharge.com" # the base hostname & protocol for your site

# Build settings
markdown: redcarpet
source: src
destination: _site
keep_files: ["fonts","css", "images", "js"]
encoding: utf-8
include: [_headers, _redirects]
exclude:
    - node_modules
plugins:
  - jekyll-sitemap
  - jekyll-pug

page_gen-dirs: true # uncomment, if you prefer to generate named folders
page_gen:
  - data: 'contentful.spaces.cpacharge.blogArticle'
    template: 'blog-article'
    name: 'sub_head'
    dir: 'resources/blog'
  - data: 'contentful.spaces.cpacharge.solutionL3Page'
    template: 'features-l3'
    name: 'sub_head'
    dir: 'features'

### THIS NEEDS TO BE UPDATED TO POINT TO THE CONTENTFUL SPACE
contentful:
  spaces:
    - cpacharge: # This is the name of the YAML file, call it whatever you want
        space: XXXXXXXXXXX       # Required
        access_token: XXXXXXXXXXXXXXXXXXXXXXXXXXX
        base_path: src
        cda_query:                  # Optional
          limit: 1000
```

Configuration settings for Jekyll are located in the config.yml file:

### Page Templates
`- data: 'contentful.spaces.cpacharge.blogArticle'`  
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`template: 'blog-article'`  
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`name: 'sub_head'`   

### Site Source
Where all of the source files are located.  
`source: src`

### Destination
This is the directory where the site will be generated:  
`destination: _site`

### Plugins
`- jekyll-sitemap`

### Contentful API
The config is where you will also connect to the Contentful API to pull down data needed to generate pages.



# Foundation 6
## Navigation
## XY Grid
Find the full documentation for Foundation 6 here:  
<a href="https://foundation.zurb.com/sites/docs/xy-grid.html">https://foundation.zurb.com/sites/docs/xy-grid.html</a>  


```Code
<div class="grid-x">
  <div class="cell">full width cell</div>
  <div class="cell">full width cell</div>
</div>
<div class="grid-x">
  <div class="cell small-6">6 cells</div>
  <div class="cell small-6">6 cells</div>
</div>
<div class="grid-x">
  <div class="cell medium-6 large-4">12/6/4 cells</div>
  <div class="cell medium-6 large-8">12/6/8 cells</div>
</div>
```
###Basics

The structure of XY grid uses .grid-x, .grid-y, and .cell as its base. Without defining a gutter type the cells will simply split up the space without any gutters.


# Contentful
## Content Models
###The basics
In short, a content model gives structure and organization to your content. Within your overall content model, you’ll have individual content types. You can think of each content type as an outline for your content; it tells you what data will be contained within each individual entry. You can also think of the content type as the ‘stencil’ for the ‘drawing’ that will be your entry.

<img src="/images/content-models.png" />  

<img src="/images/about-page.png" />


Log in to <a href="https://app.contentful.com">app.contentful.com</a> and navigate to "content models" to get started.
## Jekyll Contentful
Jekyll-Contentful-Data-Import is a Jekyll extension to use the Jekyll static site generator together with Contentful. It is powered by the Contentful Ruby Gem.  
To get the latest data pulled down to your local version run `jekyll contentful` in your terminal.  
<a href="">https://github.com/contentful/jekyll-contentful-data-import</a>

## data.yaml
###Entry mapping
The Jekyll-Contentful-Data-Import extension will transform every fetched entry before storing it as a yaml file in the local data folder. If a custom mapper is not specified a default one will be used.  
The default mapper will map fields, assets and linked entries.  

<img src="/images/data.png" />
## Liquid Variables
## Image Hosting

# Marketo
## Forms
## Landing Pages
## Internal Email
## Email Marketing

# Drift / Chat

# Netlify
## Continuous Integration
## Deployments
## A/B Testing
## Branch Previews
## Dev Environments


# Bitbucket / GIT
## Workflow
## Pull requests
## QA

# Jira
## Issue Types
## Approval Process

# Analytics
## Google Analytics
## Tag Manager

# VT
## Sign Up Forms
